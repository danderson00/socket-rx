# @x/socket.rx Sample

To start, in one terminal window, execute:

```shell
yarn
yarn start
```

In another:

```shell
node host
```