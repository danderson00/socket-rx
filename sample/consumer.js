import socket from '@x/socket'
import rxFeature from '@x/socket.rx'

const url = `ws://${window.location.hostname}:8081/`

window.onload = async () => {
  const { getServerTime } = await socket({ url })
    .useFeature('reestablishSessions')
    .useFeature(rxFeature())
    .connect()

  const serverTime = await getServerTime()
  serverTime.subscribe(
    value => document.body.innerText = new Date(value).toString()
  )
}