const host = require('@x/socket')
const rxFeature = require('@x/socket.rx')
const Websocket = require('ws')
const { interval, map } = require('rxjs')

const timer = interval(1000)
  .pipe(map(() => Date.now()))

const api = {
  getServerTime: () => timer
}

host({ server: new Websocket.Server({ port: 8081 }) })
  .useFeature(rxFeature())
  .useApi(api)
