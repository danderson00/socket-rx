const observable = require('@x/observable')
const { Observable } = require('rxjs');

module.exports = () => () => ({
  name: 'rx',
  initialise: () => ({
    middleware: async ({ next }) => {
      const result = await next()
      if(observable.isObservable(result)) {
        if(result().type === 'x') {
          return observable(
            publish => {
              result.subscribe(publish)
              return result.disconnect
            },
            { initialValue: result().value }
          )

        } else if(result().type === 'rx') {
          return new Observable(subscriber => {
            result.subscribe(({ cb, value, error }) => {
              switch(cb) {
                case 'next': return subscriber.next(value)
                case 'error': return subscriber.error(error)
                case 'complete': return subscriber.complete()
                default: throw new Error('Unknown callback')
              }
            })
            return result.disconnect
          })

        }
      }

      return result
    }
  })
})

