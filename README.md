# @x/socket.rx

ReactiveX support for @x/socket 

## Distributing RxJS Observables

This feature brings all the goodness of super-reliable observable APIs from 
[@x/socket](https://www.npmjs.com/package/@x/socket) to 
[ReactiveX](https://rxjs.dev/) observables.

Given a simple API with a single function that returns an observable that emits the server time roughly every second:

```javascript
const host = require('@x/socket')
const rxFeature = require('@x/socket.rx')
const Websocket = require('ws')
const { interval, map } = require('rxjs')

const timer = interval(1000)
  .pipe(map(() => Date.now()))

const api = {
  getServerTime: () => timer
}

host({ server: new Websocket.Server({ port: 8081 }) })
  .useFeature(rxFeature())
  .useApi(api)
```

We can easily connect to the API and start making seamless async calls:

```javascript
import socket from '@x/socket'
import rxFeature from '@x/socket.rx'

const url = `ws://${window.location.hostname}:8081/`

window.onload = async () => {
  const { getServerTime } = await socket({ url })
    .useFeature('reestablishSessions')
    .useFeature(rxFeature())
    .connect()

  const serverTime = await getServerTime()
  serverTime.subscribe(
    value => document.body.innerText = new Date(value).toString()
  )
}
```

A working sample of this code can be found [here](https://gitlab.com/danderson00/socket-rx/-/tree/main/sample).

For the full set of features, including authentication, client identification and more, check out the 
[@x/socket documentation](https://gitlab.com/danderson00/socket).

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
