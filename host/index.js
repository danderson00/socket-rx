const observable = require('@x/observable')
const { isObservable: isRxObservable } = require('rxjs')

module.exports = () => () => {
  return {
    name: 'rx',
    middleware: async ({ next }) => {
      const result = await next()
      if(observable.isObservable(result)) {
        return observable(
          publish => result.subscribe(publish).unsubscribe,
          { initialValue: { type: 'x', value: result() } }
        )

      } else if(isRxObservable(result)) {
        return observable(
          publish => {
            const subscription = result
              .subscribe({
                next: value => publish({ cb: 'next', value }),
                error: error => publish({ cb: 'error', error }),
                complete: () => publish({ cb: 'complete' })
              })
            return () => subscription.unsubscribe()
          },
          { initialValue: { type: 'rx' } }
        )

      } else {
        return result
      }
    }
  }
}