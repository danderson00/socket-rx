const hostFeature = require('../host')
const consumerFeature = require('../consumer')
const hostModule = require('@x/socket/host')
const consumerModule = require('@x/socket/consumer')
const { subject, isObservable } = require('@x/observable')
const { Subject, isObservable: isRxObservable } = require('rxjs')
const WebSocket = require('ws')

const setup = test => async () => {
  const server = new WebSocket.Server({ port: 1234 })
  const o = subject({ initialValue: '1' })
  const rxo = new Subject()
  hostModule({ server, log: { level: 'fatal' } })
    .useApi({
      value: () => 'test',
      x: () => o,
      rx: () => rxo
    })
    .useFeature(hostFeature({ provider: 'local', storagePath: __dirname + '/uploads' }))
  const api = await consumerModule({ socketFactory: () => new WebSocket('ws://localhost:1234') })
    .useFeature(consumerFeature())
    .connect()

  try {
    await test(api, { o, rxo })
  } finally {
    server.close()
  }
}

test("static value", setup(async ({ value }) => {
  expect(await value()).toBe('test')
}))

test("@x observable", setup(async ({ x }, { o }) => {
  const o2 = await x()
  expect(isObservable(o2)).toBe(true)
  expect(o2()).toBe('1')
  o.publish('2')
  await new Promise(r => setTimeout(r, 5))
  expect(o2()).toBe('2')
}))

test("rxjs observable next", setup(async ({ rx }, { rxo }) => {
  const next = jest.fn()

  const o2 = await rx()
  expect(isRxObservable(o2)).toBe(true)
  o2.subscribe({ next })
  rxo.next('1')
  await new Promise(r => setTimeout(r, 5))

  expect(next.mock.calls).toEqual([['1']])
}))

test("rxjs observable error", setup(async ({ rx }, { rxo }) => {
  const error = jest.fn()

  const o2 = await rx()
  expect(isRxObservable(o2)).toBe(true)
  o2.subscribe({ error })
  rxo.error('error')
  await new Promise(r => setTimeout(r, 5))

  expect(error.mock.calls).toEqual([['error']])
}))

test("rxjs observable complete", setup(async ({ rx }, { rxo }) => {
  const complete = jest.fn()

  const o2 = await rx()
  expect(isRxObservable(o2)).toBe(true)
  o2.subscribe({ complete })
  rxo.complete()
  await new Promise(r => setTimeout(r, 5))

  expect(complete.mock.calls).toEqual([[]])
}))
